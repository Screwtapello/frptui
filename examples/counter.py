#!/usr/bin/env python3
import collections
import curses
import sys
import frptui
from frptui import tui


class Model(collections.namedtuple("Model", "counter, focus")):
    __slots__ = ()

    def increment(self):
        return self._replace(counter=self.counter + 1)

    def decrement(self):
        return self._replace(counter=self.counter - 1)

    def focus_left(self):
        return self._replace(focus=0)

    def focus_right(self):
        return self._replace(focus=2)


def render(model):
    return tui.HBox(
        tui.Label(
            "<->" if model.focus == 0 else " - ",
            handlers={
                " ": model.decrement,
                "\n": model.decrement,
                "\t": model.focus_right,
                curses.KEY_RIGHT: model.focus_right,
            },
        ),
        tui.Label(str(model.counter)),
        tui.Label(
            "<+>" if model.focus == 2 else " + ",
            handlers={
                " ": model.increment,
                "\n": model.increment,
                "\t": model.focus_left,
                curses.KEY_LEFT: model.focus_left,
            },
        ),
        focus_index = model.focus,
        handlers={
            "q": lambda: sys.exit(0),
            "x": lambda: sys.exit(0),
        },
    )

if __name__ == "__main__":
    frptui.run_app(Model(counter=0, focus=0), render)
