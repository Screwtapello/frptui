"""
A Text User-Interface based on Functional Reactive Programming.

You probably want to use `run_app()`.
"""
import curses
from frptui import frp


__all__ = ["run_app", "run_app_on_screen"]


def run_app_on_screen(initial_model, render, screen):
    """
    Run the given application on the given screen, forever.

    ``initial_model`` should be an instance of some type representing
    the application state.

    ``render`` should be a callable that takes a model instance
    and returns a `frptui.tui.Widget` instance that represents the
    application state. Any keyboard event handlers attached to the
    ``Widget`` should be callables that take no arguments, and return
    a new model instance.

    ``screen`` should be a `curses.window` instance, as returned by
    `curses.initscr()`.
    """

    @frp.stateful_stream_function
    def curses_events():
        # Downstream needs a width and height so it can draw something
        # before it waits for input.
        height, width = screen.getmaxyx()
        yield width, height, None

        while True:
            key = screen.get_wch(0, 0)
            if key == curses.KEY_RESIZE:
                height, width = screen.getmaxyx()
                key = None

            yield width, height, key

    @frp.stateful_stream_function
    def render_model(curses_state):
        # Copy the outer variable into a local variable we can update.
        model = initial_model
        widget = render(model)

        while True:
            width, height, key = curses_state

            if key is not None:
                handler = widget.handle_key(key)

                if handler is not tui.NOT_HANDLED:
                    model = handler()

            widget = render(model)
            canvas = widget.paint(width, height)

            (curses_state,) = yield canvas

    @frp.stream_function
    def curses_output(canvas):
        for y in range(canvas.height):
            for x in range(canvas.width):
                if x == canvas.width - 1 and y == canvas.height - 1:
                    # curses can't draw to the bottom-right corner of
                    # the screen.
                    pass
                else:
                    screen.addstr(y, x, canvas[x, y])

    events = curses_events()
    canvases = render_model(events)
    sink = curses_output(canvases)

    phase = 0
    while True:
        sink.poll(phase)

        # Toggle phase so the next pass will calculate new values.
        phase = phase ^ 1


def run_app(initial_model, render):
    """
    Like `run_app_on_screen()`, but uses `curses.wrapper()` to get the screen.
    """
    curses.wrapper(
        lambda screen: run_app_on_screen(initial_model, render, screen)
    )
