"""
A text-based user-interface framework.

Unlike other user-interface frameworks you might have used, this is
designed around the idea of immutability: rather than modifying widgets
in-place as the application changes state (and therefore requiring
"controller" code to bi-directionally synchronise the UI with the
application model), every time something happens, the UI is recreated
from scratch from the application model. Therefore, no synchronisation
is required, and the system as a whole is simpler.
"""
NOT_HANDLED = object()


class Canvas:
    """
    A rectangular region of printable ASCII characters.

        >>> c = Canvas(3, 2)
        >>> c[0, 0] = "A"
        >>> c[2, 1] = "B"
        >>> print(c)
        A  
          B

    """

    def __init__(self, width, height):
        self._rows = [[" "] * width for _ in range(height)]

    @property
    def width(self):
        return len(self._rows[0])

    @property
    def height(self):
        return len(self._rows)

    def __str__(self):
        return "\n".join("".join(row) for row in self._rows)

    def __getitem__(self, key):
        x, y = key
        return self._rows[y][x]

    def __setitem__(self, key, value):
        x, y = key
        self._rows[y][x] = value

    def stamp(self, other, at_x, at_y):
        """
        Draw another canvas onto this one.

        The top-left of the other canvas will be positioned at
        (``at_x``, ``at_y``) on this canvas. ``at_x`` and ``at_y``
        can be negative. Any portion of the other canvas that lies
        outside the bounds of this canvas will be clipped.
        """
        xs = [
            (other_x, self_x)
            for (other_x, self_x) in zip(
                range(0, other.width),
                 range(at_x, self.width),
            )
            if 0 <= self_x < self.width
        ]
        ys = [
            (other_y, self_y)
            for (other_y, self_y) in zip(
                range(0, other.height),
                 range(at_y, self.height),
            )
            if 0 <= self_y < self.height
        ]

        for other_y, self_y in ys:
            for other_x, self_x in xs:
                self[self_x, self_y] = other[other_x, other_y]


class Widget:
    """
    Base-class for user-interface components.

    Pass a dictionary mapping ``curses.KEY_*`` constants to values,
    if you want a widget to handle key-press events.
    """

    def __init__(self, handlers=None):
        self._handlers = handlers if handlers is not None else {}

    def paint(self, width, height):
        """
        Return a `Canvas` of the given dimensions, with this widget on it.

        By default, returns a blank `Canvas`.
        """
        return Canvas(width, height)

    def handle_key(self, key_event):
        """
        Return the value associated with the given key.

        Keys can be associated with values by passing a dict of handlers
        to the constructor.

        If a key has no associated value, the special value
        ``NOT_HANDLED`` is returned, since `None` might plausibly be
        a value somebody wants to store.
        """
        return self._handlers.get(key_event, NOT_HANDLED)


class Padding(Widget):
    """
    A `Widget` that fills itself with a single character.

    By default, the fill character is a space, but you can pass any
    character to the constructor as ``fill_char``.
    """

    def __init__(self, fill_char=" ", **kwargs):
        super().__init__(**kwargs)
        self._fill_char = fill_char

    def paint(self, width, height):
        canvas = super().paint(width, height)
        for y in range(height):
            for x in range(width):
                canvas[x, y] = self._fill_char
        return canvas


class HBox(Widget):
    """
    A `Widget` that stacks child-widgets left-to-right.

    All positional arguments to the constructor are child widgets. Child
    widgets are each allocated an equal slice of this widget's width,
    except the last child which will be wider if the total width is not
    an integer multiple of the number of children.

    The keyword argument ``focus_index`` should be the 0-based index of
    the child with keyboard focus. Keyboard events will first be
    dispatched to this child; if the child does not handle the event,
    this widget will try to handle it.
    """

    def __init__(self, first_child, *children, focus_index=0, **kwargs):
        super().__init__(**kwargs)
        self._children = (first_child,) + children
        self._focus_index = focus_index
        assert 0 <= focus_index < len(self._children)

    def paint(self, width, height):
        canvas = super().paint(width, height)
        child_width = width // len(self._children)
        for i, child in enumerate(self._children[:-1]):
            child_canvas = child.paint(child_width, height)
            canvas.stamp(child_canvas, i * child_width, 0)
            
        # Last child gets the remainder of the width
        child_canvas = self._children[-1].paint(
            canvas.width - (len(self._children) - 1) * child_width,
            canvas.height,
        )
        canvas.stamp(child_canvas, (len(self._children) - 1) * child_width, 0)
        return canvas

    def handle_key(self, key_event):
        focussed_child = self._children[self._focus_index]

        result = focussed_child.handle_key(key_event)
        if result == NOT_HANDLED:
            result = super().handle_key(key_event)

        return result


class Label(Widget):
    """
    A `Widget` that draws text in the center of the available area.

    Pass the text to draw to the constructor.
    """

    def __init__(self, text, **kwargs):
        super().__init__(**kwargs)
        self._text = text

    def paint(self, width, height):
        canvas = super().paint(width, height)

        y = height // 2
        text_width = len(self._text)
        start_x = (width - text_width) // 2
        xs = zip(
            range(start_x, start_x + text_width),
            range(text_width),
        )
        for canvas_x, text_x in xs:
            canvas[canvas_x, y] = self._text[text_x]
        
        return canvas
