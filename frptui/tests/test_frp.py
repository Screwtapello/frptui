import unittest


from frptui import frp


class TestLiteralStream(unittest.TestCase):

    def test_poll_with_same_phase_returns_same_value(self):
        src = frp.LiteralStream(5, 9, 32, 17)

        self.assertEqual(src.poll(0), 5)
        self.assertEqual(src.poll(0), 5)
        self.assertEqual(src.poll(0), 5)
        self.assertEqual(src.poll(0), 5)

    def test_poll_with_different_phase_returns_next_value(self):
        src = frp.LiteralStream(5, 9, 32, 17)

        self.assertEqual(src.poll(0), 5)
        self.assertEqual(src.poll(1), 9)
        self.assertEqual(src.poll(0), 32)
        self.assertEqual(src.poll(1), 17)


@frp.stream_function
def add_two(value):
    """
    Adds two to each value in the input stream.
    """
    return value + 2


@frp.stream_function
def multiply(x, y):
    """
    Multiplies the values from each stream together.
    """
    return x * y


class TestStreamFunction(unittest.TestCase):

    def test_do_calculation(self):
        src = frp.LiteralStream(5, 2)

        two_adder = add_two(src)

        # If we poll the stream, we get the expected result.
        self.assertEqual(two_adder.poll(0), 7)

        # If we poll the stream again in the same phase, we get the same
        # result.
        self.assertEqual(two_adder.poll(0), 7)

        # If we poll the stream again in the next phase, it will
        # recalculate.
        self.assertEqual(two_adder.poll(1), 4)

    def test_calculate_multiple_inputs(self):
        src1 = frp.LiteralStream(5, 4, 3)
        src2 = frp.LiteralStream(7, 6, 5)

        multiplyer = multiply(src1, src2)

        # If we poll the stream, we get the expected result.
        self.assertEqual(multiplyer.poll(0), 35)

        # If we poll it again in the same phase, we get the same result.
        self.assertEqual(multiplyer.poll(0), 35)

        # If we poll it in the next phase, we get the next result.
        self.assertEqual(multiplyer.poll(1), 24)

    def test_decorator_copies_metadata(self):
        self.assertEqual(
            add_two.__module__,
            self.__class__.__module__,
        )
        self.assertEqual(add_two.__name__, "add_two")
        self.assertEqual(add_two.__qualname__, "add_two")
        self.assertEqual(
            add_two.__doc__.strip(),
            "Adds two to each value in the input stream.",
        )


@frp.stateful_stream_function
def five_sample_moving_average(value):
    """
    The average of the last 5 samples of the input stream.
    """
    samples = []
    while True:
        samples.append(value)
        if len(samples) > 5:
            samples.pop(0)

        (value,) = yield sum(samples) / len(samples)


class TestStatefulStreamFunction(unittest.TestCase):

    def test_stateful_stream_handling(self):
        src = frp.LiteralStream(5, 5, 0, 0, 0, 0, 0)

        avg = five_sample_moving_average(src)

        # With only the initial value, the moving average is the same
        # as the input.
        self.assertAlmostEqual(avg.poll(0), 5.0, delta=0.01)

        # With a second sample identical to the first, the moving average
        # is still the same.
        self.assertAlmostEqual(avg.poll(1), 5.0, delta=0.01)

        # If the source changes, the moving average changes too, but
        # not by as much.
        self.assertAlmostEqual(avg.poll(0), 3.33, delta=0.01)

        # If we poll again in the same phase, we should get the same
        # answer, rather than adding another sample to the buffer.
        self.assertAlmostEqual(avg.poll(0), 3.33, delta=0.01)

        # Add more zeroes to the buffer until we shift out the last 5.
        self.assertAlmostEqual(avg.poll(1), 2.50, delta=0.01)
        self.assertAlmostEqual(avg.poll(0), 2.00, delta=0.01)
        self.assertAlmostEqual(avg.poll(1), 1.00, delta=0.01)
        self.assertAlmostEqual(avg.poll(0), 0.00, delta=0.01)

    def test_decorator_copies_metadata(self):
        self.assertEqual(
            five_sample_moving_average.__module__,
            self.__class__.__module__,
        )
        self.assertEqual(
            five_sample_moving_average.__name__,
            "five_sample_moving_average",
        )
        self.assertEqual(
            five_sample_moving_average.__qualname__,
            "five_sample_moving_average",
        )
        self.assertEqual(
            five_sample_moving_average.__doc__.strip(),
            "The average of the last 5 samples of the input stream.",
        )
