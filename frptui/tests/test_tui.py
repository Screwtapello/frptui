import curses
import unittest


from frptui import tui


class TestCanvas(unittest.TestCase):

    def test_dimensions(self):
        canvas = tui.Canvas(5, 7)
        self.assertEqual(canvas.width, 5)
        self.assertEqual(canvas.height, 7)

    def test_drawing(self):
        canvas = tui.Canvas(2, 2)

        canvas[0, 0] = "A"
        canvas[1, 0] = "B"
        canvas[0, 1] = "C"
        canvas[1, 1] = "D"

        self.assertEqual(canvas[0, 0], "A")
        self.assertEqual(canvas[1, 0], "B")
        self.assertEqual(canvas[0, 1], "C")
        self.assertEqual(canvas[1, 1], "D")

        self.assertEqual(
            str(canvas),
            "\n".join([
                "AB",
                "CD",
            ]),
        )

    def test_stamp(self):
        master = tui.Canvas(3, 3)
        
        stamp = tui.Canvas(2, 2)
        stamp[0, 0] = "A"
        stamp[1, 0] = "B"
        stamp[0, 1] = "C"
        stamp[1, 1] = "D"

        # Stamp the stamp in the bottom right of the canvas.
        master.stamp(stamp, 1, 1)
        self.assertEqual(
            str(master),
            "\n".join([
                "   ",
                " AB",
                " CD",
            ]),
        )

        # Stamp the stamp in the top left. We overwrite the centre cell.
        master.stamp(stamp, 0, 0)
        self.assertEqual(
            str(master),
            "\n".join([
                "AB ",
                "CDB",
                " CD",
            ]),
        )

        # Stamp in the top right.
        # The right hand side of the stamp is clipped.
        master.stamp(stamp, 2, 0)
        self.assertEqual(
            str(master),
            "\n".join([
                "ABA",
                "CDC",
                " CD",
            ]),
        )

        # Stamp in the bottom left.
        # The bottom of the stamp is clipped.
        master.stamp(stamp, 0, 2)
        self.assertEqual(
            str(master),
            "\n".join([
                "ABA",
                "CDC",
                "ABD",
            ]),
        )

        # Stamp above the top left.
        # The top and left of the stamp is clipped.
        master.stamp(stamp, -1, -1)
        self.assertEqual(
            str(master),
            "\n".join([
                "DBA",
                "CDC",
                "ABD",
            ]),
        )


class TestWidget(unittest.TestCase):

    def test_paint_produces_blank_canvas(self):
        widget = tui.Widget()
        canvas = widget.paint(3, 2)

        self.assertEqual(
            str(canvas),
            "\n".join([
                "   ",
                "   ",
            ]),
        )

    def test_key_events_not_handled_by_default(self):
        widget = tui.Widget()

        self.assertEqual(widget.handle_key("a"), tui.NOT_HANDLED)
        self.assertEqual(widget.handle_key(curses.KEY_BACKSPACE), tui.NOT_HANDLED)

    def test_key_events_can_be_handled(self):
        widget = tui.Widget(handlers={curses.KEY_BACKSPACE: "bodega"})

        self.assertEqual(widget.handle_key("a"), tui.NOT_HANDLED)
        self.assertEqual(widget.handle_key(curses.KEY_BACKSPACE), "bodega")


class TestPadding(unittest.TestCase):

    def test_paint_with_fill_char(self):
        paddingA = tui.Padding("A")
        canvas = paddingA.paint(3, 3)

        # Make sure we have a non-blank Canvas
        self.assertEqual(
            str(canvas),
            "\n".join([
                "AAA",
                "AAA",
                "AAA",
            ]),
        )

    def test_fill_char_defaults_to_blank(self):
        canvas = tui.Padding().paint(3, 3)

        # Now the canvas should be blank again.
        self.assertEqual(
            str(canvas),
            "\n".join([
                "   ",
                "   ",
                "   ",
            ]),
        )


class TestHBox(unittest.TestCase):

    def test_paint_one_child(self):
        padding = tui.Padding("A")
        hbox = tui.HBox(padding)
        canvas = hbox.paint(2, 2)
        self.assertEqual(
            str(canvas),
            "\n".join([
                "AA",
                "AA",
            ]),
        )

    def test_paint_two_children(self):
        paddingA = tui.Padding("A")
        paddingB = tui.Padding("B")
        hbox = tui.HBox(paddingA, paddingB)
        canvas = hbox.paint(5, 2)

        self.assertEqual(
            str(canvas),
            "\n".join([
                "AABBB",
                "AABBB",
            ]),
        )

    def test_key_events_go_to_focussed_child(self):
        paddingA = tui.Padding("A", handlers={"x": "A"})
        paddingB = tui.Padding("B", handlers={"x": "B"})
        paddingC = tui.Padding("C", handlers={"x": "C"})

        hbox = tui.HBox(paddingA, paddingB, paddingC, focus_index=2)
        self.assertEqual(hbox.handle_key("x"), "C")

    def test_focus_defaults_to_first_child(self):
        paddingA = tui.Padding("A", handlers={"x": "A"})
        paddingB = tui.Padding("B", handlers={"x": "B"})
        paddingC = tui.Padding("C", handlers={"x": "C"})

        hbox = tui.HBox(paddingA, paddingB, paddingC)
        self.assertEqual(hbox.handle_key("x"), "A")

    def test_hbox_handles_unhandled_key_events(self):
        paddingA = tui.Padding("A", handlers={"a": "paddingA"})
        paddingB = tui.Padding("B", handlers={"b": "paddingB"})
        hbox = tui.HBox(paddingA, handlers={"h": "hbox"})

        # If we send "a", we'll get a result from paddingA
        self.assertEqual(hbox.handle_key("a"), "paddingA")

        # If we send "b", we'll get nothing, because paddingB is not focussed.
        self.assertEqual(hbox.handle_key("b"), tui.NOT_HANDLED)

        # If we send "h", paddingA won't handle it, but the hbox can.
        self.assertEqual(hbox.handle_key("h"), "hbox")


class TestLabel(unittest.TestCase):

    def test_shrinkwrapped_canvas(self):
        label = tui.Label("hello")
        canvas = label.paint(5, 1)
        self.assertEqual(str(canvas), "hello")

    def test_label_centred_on_larger_canvas(self):
        label = tui.Label("hello")
        canvas = label.paint(8, 4)
        self.assertEqual(
            str(canvas),
            "\n".join([
                "        ",
                "        ",
                " hello  ",
                "        ",
            ]),
        )
