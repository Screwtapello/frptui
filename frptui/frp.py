"""
A Functional Reactive Programming framework for Python.
"""
from itertools import cycle


def class_wraps(func):
    """
    Like functools.wraps(), but it can decorate a class.

    Used to define decorators that decorate a function with a class::

        >>> def add_methods(func):
        ...     @class_wraps(func)
        ...     class Inner:
        ...         def call_with_five(self):
        ...             return func(5)
        ...     return Inner
        ...
        >>> @add_methods
        ... def add_two(other):
        ...     return 2 + other
        ...
        >>> two_adder = add_two() # constructor takes no args
        >>> two_adder.call_with_five()
        7
        >>> two_adder.__name__
        "add_two"

    """
    def inner(cls):
        cls.__module__ = func.__module__
        cls.__name__ = func.__name__
        cls.__qualname__ = func.__qualname__
        cls.__doc__ = func.__doc__

        return cls

    return inner


class IStream:
    "The interface that all streams must provide."

    def poll(self, phase):
        """
        Returns the current output value of this stream.

        ``phase`` is a token that ensures repeated polls return consistent
        values.  If the value of ``phase`` is equal to the value given
        the previous time ``.poll()`` was called, this *must* return
        the same value as the previous time. If the value of ``phase``
        is different than it was the previous time, the caller wants
        the next value in the stream.

        If this stream obtains values from other streams, it should pass
        the current ``phase`` along when polling them.
        """


class BaseStream(IStream):
    "A base class that handles stream phases for you."

    def __init__(self):
        self._last_phase = None
        self._last_output = None

    def _poll(self, phase):
        "Override this to implement the actual stream function"

    def poll(self, phase):
        if phase != self._last_phase:
            self._last_phase = phase
            self._last_output = self._poll(phase)

        return self._last_output


class LiteralStream(BaseStream):
    "A stream that endlessly repeats the values given to the constructor."
    def __init__(self, *items):
        super().__init__()
        self._items = cycle(items)

    def _poll(self, _):
        return next(self._items)


def stream_function(func):
    "Decorates a pure function, making a stream function."

    @class_wraps(func)
    class Inner(BaseStream):
        # We don't know in advance how many inputs func() takes,
        # so we'll take any number.
        def __init__(self, *inputs):
            super().__init__()
            self._inputs = inputs

        def _poll(self, phase):
            # Poll all the input streams to get their current values.
            current_input_values = [
                each.poll(phase)
                for each in self._inputs
            ]

            # Whatever func() returns, given these values,
            # is the current output stream value.
            return func(*current_input_values)

    return Inner


def stateful_stream_function(func):
    "Decorates a generator function, making a stream function."

    @class_wraps(func)
    class Inner(BaseStream):
        # We don't know in advance how many inputs func() takes,
        # so we'll take any number.
        def __init__(self, *inputs):
            super().__init__()
            self._inputs = inputs

            # We need to store the generator object returned
            # when we call func(), so we can repeatedly resume it.
            self._generator = None

        def _poll(self, phase):
            # Poll all the input streams to get their current values.
            current_input_values = [
                each.poll(phase)
                for each in self._inputs
            ]

            # If we have not yet created the generator object...
            if self._generator is None:
                # ...create it, passing the initial input values...
                self._generator = func(*current_input_values)
                # ...then execute up to the first yield expression.
                # The yielded value is our first output.
                return self._generator.send(None)

            # If we already have a generator object,
            # pass in the next input values and execute up to
            # the next yield expression. The yielded value is
            # our next output.
            return self._generator.send(current_input_values)

    return Inner
