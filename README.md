Functional Reactive Programming and the Elm Architecture in Python
==================================================================

This library is an example and exploration of
Functional Reactive Programming in Python,
and also an attempt to implement something like
the Elm Architecture for a text-based user-interface.

Here's all the interesting bits:

- [frptui.frp](frptui/frp.py) is the actual FRP framework,
  the base classes and function decorators.
- [frptui.tui](frptui/tui.py) is a very simple widget library,
  where each widget can route keyboard events to a handler,
  and render itself to a canvas. No support for colour or
  anything but printable ASCII, just text labels and a container that
  stacks its contents horizontally.
- [frptui](frptui/__init__.py), the top-level of the library,
  builds on `frptui.frp` and `frptui.tui`. It contains helper functions
  that together approximate the Elm Architecture. If you have a class
  that represents the data model of your application, and a function
  that turns a model instance into a tree of `tui.Widget` widgets,
  these functions will run the application and display its output.
- [examples/counter.py](examples/counter.py) is an application that
  mirrors the first example in the Elm documentation: a counter with
  "increment" and "decrement" buttons. Space or Enter will activate
  the focussed button, Tab and arrow keys will move the keyboard focus,
  `q` and `x` will exit.
